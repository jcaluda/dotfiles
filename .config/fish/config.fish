alias l 'ls'
alias vi 'vim'
alias fconf 'cd ~/.config/fish'

set fish_greeting ""
set -U fish_prompt_pwd_dir_length 0

function reload
    if source ~/.config/fish/config.fish
        echo "config.fish reloaded"
    end
end

function start_tmux
    if type tmux > /dev/null
        #if not inside a tmux session, and if no session is started, start a new session
        if test -z "$TMUX" ; and test -z $TERMINAL_CONTEXT
            tmux -2 attach; or tmux -2 new-session
        end
    end
end

function start_agent
    echo "Initializing new SSH agent ..."
    ssh-agent -c | sed 's/^echo/#echo/' > $SSH_ENV
    echo "succeeded"
    chmod 600 $SSH_ENV
    . $SSH_ENV > /dev/null
    ssh-add
end

function test_identities
    ssh-add -l | grep "The agent has no identities" > /dev/null
    if [ $status -eq 0 ]
        ssh-add
        if [ $status -eq 2 ]
            start_agent
        end
    end
end

function handle_ssh_agent
    if [ -n "$SSH_AGENT_PID" ]
        ps -ef | grep $SSH_AGENT_PID | grep ssh-agent > /dev/null
        if [ $status -eq 0 ]
            test_identities
        end
    else
        if [ -f $SSH_ENV ]
            . $SSH_ENV > /dev/null
        end
        ps -ef | grep $SSH_AGENT_PID | grep -v grep | grep ssh-agent > /dev/null
        if [ $status -eq 0 ]
            test_identities
        else
            start_agent
        end
    end
end

switch (uname)
    case Linux
        if grep -q Microsoft /proc/version
            if [ $NAME = 'tardis' ]
                set -g PROJECTS_HOME "/mnt/c/Jason"
            else if [ $NAME = 'venom' ]
                set -g PROJECTS_HOME "/mnt/f/Jason"
            end

            alias repos 'cd $PROJECTS_HOME/repos'

            setenv SSH_ENV $HOME/.ssh/environment

            set -x VAGRANT_WSL_ENABLE_WINDOWS_ACCESS "1"
            set -x VAGRANT_WSL_WINDOWS_ACCESS_USER_HOME_PATH $PROJECTS_HOME

            if test -f ~/.dircolors/dircolors.ansi-dark
                set -gx LS_COLORS (dircolors -c ~/.dircolors/dircolors.ansi-dark | string replace -r 'setenv LS_COLORS \'(.*)\'' '$1')
            end

            eval (python -m virtualfish)

            handle_ssh_agent
            start_tmux
        end
    case Darwin
        set -x VIRTUALFISH_HOME "/Users/jasoncaluda/venv"
        eval (python3 -m virtualfish)
        start_tmux
    case '*'
        echo Hi, stranger!
end
