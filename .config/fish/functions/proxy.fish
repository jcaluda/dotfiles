set -g PROXY_ENV http_proxy HTTP_PROXY https_proxy HTTPS_PROXY ftp_proxy FTP_PROXY all_proxy ALL_PROXY VAGRANT_HTTP_PROXY VAGRANT_HTTPS_PROXY

function noproxy
    for envar in $PROXY_ENV
        set -e $envar
    end
    set -e no_proxy
    set -e NO_PROXY
end

function proxy
    if test -n "$1"
        set proxy_value "$1"
    else
        set proxy_value "PROXY_SERVER:PROXY_PORT"
    end

    for envar in $PROXY_ENV
        set -gx $envar $proxy_value
    end
    set -gx NO_PROXY 'ge.com'
    set -gx no_proxy $NO_PROXY
end
