#!/usr/bin/env bash

# Exit immediately if a command exits with a non-zero status
set -e

export OS=`uname -s | sed -e 's/  */-/g;y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/'`

if [ "$OS" = "linux" ]; then
    PIP_PREFIX="sudo /usr/bin/pip"
    VIRTUALENVS_HOME=$HOME/.virtualenvs
    # Install basic packages
    sudo apt-get update
	  sudo apt-get -y upgrade

    sudo apt-get install -y \
		        python-pip

    sudo apt-get -y autoremove

    # Install fish and make it the default shell
    if [ ! -f /usr/bin/fish ]; then
        sudo apt-add-repository -y ppa:fish-shell/release-2
        sudo apt-get update
        sudo apt-get -y install fish
        echo -e "\nChanging default shell for current user to fish..."
        sudo chsh -s /usr/bin/fish $USER
    fi

    # Install global Python packages
    [ ! -f /usr/local/bin/virtualenv ] && $PIP_PREFIX install virtualenv
    [ ! -d /usr/local/lib/python2.7/dist-packages/virtualfish ] && $PIP_PREFIX install virtualfish

    # Retrieve dotfiles (clone if missing or pull if exists )
    if [ -d $HOME/.dotfiles ]; then
        cd $HOME/.dotfiles && git pull
    else
        git clone https://bitbucket.org/jcaluda/dotfiles $HOME/.dotfiles
    fi

    # Create needed directories
    mkdir -p $HOME/.config/fish $VIRTUALENVS_HOME

fi

# Ensure symlinks
function ensure_link {
    test -L "$HOME/$2" || ln -s "$HOME/.dotfiles/$1" "$HOME/$2"
}

ensure_link ".config/fish/config.fish"                 ".config/fish/config.fish"
ensure_link ".config/fish/functions"                   ".config/fish/functions"
ensure_link ".gitconfig"                               ".gitconfig"
ensure_link ".tmux.conf"                               ".tmux.conf"
ensure_link ".dircolors"                               ".dircolors"

printf "Install process completed.\n"
